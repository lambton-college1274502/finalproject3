---
name: Feature request
about: Suggest a new feature or enhancement for gcip
---
<!--Inspired by https://github.com/restic/restic-->
<!--

Welcome! - We kindly ask you to fill out the issue template below.

NOTE: Not filling out the issue template needs a good reason, as otherwise it
may take a lot longer to find the problem, not to mention it can take up a lot
more time which can otherwise be spent on development. Please also take the
time to help us debug the issue by collecting relevant information, even if
it doesn't seem to be relevant to you. Thanks!

Thanks for understanding, and for contributing to the project!

-->


gcip version you are using?
-----------------------------
`pip freeze | grep gcip`

What should gcip do differently? Which functionality do you think we should add?
----------------------------------------------------------------------------------
<!--
Please describe the feature you'd like us to add here.
-->


What are you trying to do? What problem would this solve?
---------------------------------------------------------
<!--
This section should contain a brief description what you're trying to do, which
would be possible after implementing the new feature.
-->

Did gcip help you today? Did it make you happy in any way?
------------------------------------------------------------
<!--
Answering this question is not required, but if you have anything positive to share, please do so here!
Idea by Joey Hess, https://joeyh.name/blog/entry/two_holiday_stories/
-->
