# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [3.1.0]

* added PredefinedVariable CI_COMMIT_TAG_MESSAGE

## [3.0.4]

### Fixed

* fixed empty 'Rules.on_success()' rules

## [3.0.3]

### Fixed

* only add 'allow_failure' to Rule when explicitly set
* only add 'when' to Rule when explicitly set
* updated dependencies

## [3.0.2]

### Fixed

* Fixed caching issue in FullContainerSequence.

## [3.0.1]

### Fixed

* Fixed issue with curl installation in Alpine Linux.

## [3.0.0]

### Added

* New method 'set_tags()' on the Job class.

### Changed

* **BREAKING** The property 'tags' of the Job class now returns a string list
    instead of a custom datastructure.

## [2.2.0]

### Added

* Added a 'debug' option to the 'gcip.addons.container.jobs.trivy.ScanLocalImage' job.

### Fixed

* Kaniko '--build-args' now resolve environment variables

## [2.1.1]

### Fixed

* Added missing pipenv version identifier to sphinx job.

## [2.1.0]

### Changed

* Job MirrorToCodecommit: Hide unnecessary fields from superclass.
* Allow to set pipenv version to be installed on different jobs and sequences.

## [2.0.0]

### Added

* mypy job automatically installs missing types.
* pytest job accepts an argument, to configure the pytest command

### Changed

* **BREAKING** Rethinking and refactoring the changed made in release 1.x and thus updating the
  interfaces of jobs and sequences. For more information please read the developer documentation
  of the gcip.
* **BREAKING** The gcip now requires Python 3.10 (formerly 3.7)
* In use latest mypy version in mypy job.

### Changed

* Optional jobs of container sequences are always created and thus never None, but optionally added
  to the sequence.

## [1.0.0] - 2022-07-26

### Added

* This version of the gcip introduces parameter dataclasses, which makes it easy to pass
  parameters to parent classes or composed Jobs and Sequences. Please consult the (new)
  developer documentation, to read more about those parameter dataclasses.
* Added a developer documentation, describing conventions and good practices when developing
  your own library based on the gcip.
* New Job `gcip.addons.git.jobs.git.Mirror` to mirror the current repository to a remote one.
* New Job `gcip.addons.aws.jobs.codecommit.MirrorToCodecommit` to mirror the current repository
  to AWS codecommit.
* Allow the 'install_packages' script to install more than one package.

### Changed

* **BREAKING** Changed the signatures of `Job`s and `Sequence`s from the `gcip.addons` module.
  This is because those jobs and sequences make use of the new parameter dataclasses.
* **BREAKING** Moved 'install_packages' script to a module named 'package_manager'.

### Fixed

* **SECURITY** Some `PredefinedVariables` are returned unresolved as they contain sensitive
  information that should be resolved at the pipelines runtime.

## [0.28.0] - 2022-04-01

### Added

* Added `changes`, `exists` and `variables` parameter to the `Rules` class.

## [0.27.1] - 2022-04-01

* Fixed `TrivyIgnoreFileCheck` job, to exit on ingore files without content.

## [0.27.0] - 2022-02-18

### Added

* In trivy job print trivy version after executing test, to see trivy and database version.

### Changed

* Do not check contents of .trivyignore files silently. Print lines found.

## [0.26.1] - 2022-02-08

### Fixed

* The job `TrivyIgnoreFileCheck` can now handle "empty" `.trivyignore` files containing comments and empty lines.

## [0.26.0] - 2022-02-07

### Changed

* The bash script returned by `addons.python.script.pip_install_requirements` can now also handle Pipfile.lock files
* internal: Use pipenv and Pipfile for dependency management
* internal: use `setuptools-git-versioning` for creating PyPI package versions from Git tags

## [0.25.0] - 2022-01-26

### Added

* Added new addon job `TrivyIgnoreFileCheck`, this job alloes checking for existance and size of the `.trivyignore` file.
* Added `TrivyIgnoreFileCheck` to `FullContainerSequence` and `CopyContainer` sequences.

## [0.24.0] - 2022-01-25

### Changed

* **BREAKING** Pass `FullContainerSequence.image_tag` to subroutine `crane.Push`.

## [0.23.0] - 2021-11-04

### Changed

* **BREAKING** Again use caches instead of artifacts in the container sequence, but one cache per pipeline.
* Automatically replace slashes '/' and dots '.' in Cache key names.

## [0.22.0] - 2021-08-26

### Changed

* Allowed to call `initialize_needs()` and `initialize_dependencies()` on Sequence with empty parameter list,
  such that jobs explicitly does get an empty needs and dependency list.

## [0.21.0] - 2021-08-05

### Added

* Added the `dependencies` keyword to Job, JobFilter and Sequence.

### Fixed

* Fixed `set_artifacts` method on `Job` class.

## [0.20.0] - 2021-07-29

### Added

* Added `allow_failure` feature on Jobs, JobFilter and Sequences.
* Added method `set_needs` to Job, to allow setting empty needs array, as well as methods to initialize
  and override needs on Sequence level.

### Changed

* Do `apk upgrade`  in Dockerfile - security over functionality.

## [0.19.2] - 2021-07-24

### Changed

* Added need-dependencies for jobs in `gcip.addons.container.sequences.copy.CopyContainer`. This would improve speed in large pipelines.

## [0.19.1] - 2021-07-24

### Fixed

* Fixed typing of `Sequence.add_needs()` which now allows safely add sequences as needs to sequences.

## [0.19.0] - 2021-07-21

### Added

* Documented the use of [prefilled variables](https://docs.gitlab.com/ee/ci/pipelines/index.html#prefill-variables-in-manual-pipelines) in combination with gcip in the user documentation.

### Changed

* **BREAKING** The init method of the Rule class does not accept (unused) positional arguments anymore.
* **BREAKING** Replaced the `gcip.core.Image` methods `set_tag()` and `set_entrypoint()` by the methods
  `with_tag()` and `with_entrypoint()`. The new methods return an altered copy of the original Image object.
  This is because the massive confusion when using centrally provided images
  (like `gcip.addons.container.PredefinedImages`) and accidentially modifying the object for the whole
  pipeline instead for a single place.

## [0.18.0] - 2021-06-25

### Changed

* Changed `PredefinedImages` to use `tag` arguments.

### Added

* Added `tag` argument to class `Image`.

## [0.17.0] - 2021-07-14

### Added

* Most of the `gcip.Job` attributes could be set in the `__init__()`-Method.

### Fixed

* Fixed needs name generation bug where job names for 'needs' were wrongly generated. See #de9b3984 for further information.

## [0.16.0] - 2021-06-11

### Changed

* All new job classes pass arbitrary keyword arguments to their super `gcip.Job` class.
* **BREAKING** All job classes parameters `job_name` and `job_stage` were renamed to the arguments
  `name` and `stage` from the super class `gcip.Job`.
* **BREAKING** Renamed `tags` argument from `gcip.addons.aws.jobs.cdk.Bootstrap` to `resouce_tags` as `tags`
  is already a parameter of the super class.
* **BREAKING** Renamed `image` argument from `gcip.addons.container.jobs.docker.Push` to `container_image` as `image`
  is already a parameter of the super class.

## [0.16.0] - 2021-06-11

### Changed

* Added more options to `mypy` job.
* **BREAKING** Converted cdk jobs and sequences from functions to classes.
* **BREAKING** Converted python jobs and sequences from functions to classes.
* **BREAKING** Converted gitlab jobs from functions to classes.
* Moved `scripts` to own module.

### Fixed

* Fixed `mypy` job, `mypy` in versions newer `0.812` will output, that no typing stubs are installed. This leads to a failed pipeline job if there are external dependencies. Therefore the `mypy` job checks if `mypy` is installed if not it will be installed with a pinned version.

## [0.15.3] - 2021-06-02

### Fixed

* Missing `mkdir -p $TAR_PATH` in crane pull job.

## [0.15.2] - 2021-05-27

### Fixed

* Using CI_COMMIT_REF_SLUG environment variable in crane, kankio to create the tag if none is given doesn't allow e.g. doted tages like `3.13` the SLUG will change the `.` to `-` so that the tag looks like `3-13`. With this fix the tag will be exactly be named after the Branch/Ref name.

## [0.15.1] - 2021-05-21

### Fixed

* Missing `cache` property on `gcip.addons.container.sequences.*`.

## [0.15.0] - 2021-05-19

### Added

* Added contextmanager methods to Pipeline class.

### Changed

* Changed container addons from simple functions to more enhanced objects. For a comprehensive explanation about this change see MR !113.

## [0.14.1] - 2021-05-17

### Fixed

* In accordance to [MR-105](https://gitlab.com/dbsystel/gitlab-ci-python-library/-/merge_requests/105), there was the needs name generation missing. The needs job name was generated with the old style. Job name after Stage, now it is Job name before stage. Most relevant text comes first for needs too.

## [0.14.0] - 2021-05-17

### Changed

* **BREAKING** Removed obsolete `gcip.tools.evaluate_git_tag_pep404_conformity.py` file.
* #43 - `gcip.addons.container.DockerClientConfig.add_raw()` does now deep merge given raw configuration dict.
* **BREAKING**  Made `gcip.core.job.Job.copy()` private, as it is only intended for internal use.

### Fixed

* DockerClientConfig used in `gcip.addons.container.seqences.*` was modified after passing object to function, that leads to wrong path's after first modification in `kaniko` job.

## [0.13.0] - 2021-05-11

### Added

* #86 - Added possibility to reference `pipeline`s in `Need`s.
* Added `Sequence.find_job()` to find all jobs in a Sequence (or Pipeline) with matching attributes. Returned
  jobs could then be modified.

### Changed

* Changed order of job name components. Most relevant text comes first. See discussion in [MR-105](https://gitlab.com/dbsystel/gitlab-ci-python-library/-/merge_requests/105).

## [0.12.1] - 2021-05-10

### Fixed

* #85 - using `CI_PROJECT_DIR` as artifact path only, raises an error

## [0.12.0] - 2021-05-07

### Added

* Added options to the CDK deploy job and sequence making `--strict` optional.

## [0.11.0] - 2021-05-07

### Added

* Added auto download for `gcip.addons.security.sops.sops_export_decrypted_values`.
* Added new container sequence to allow copying a container image between registries.

### CHANGED

* Removed `name` argument from `gcip.addons.container.sequence.build.full_container_sequence` jobs.

## [0.10.0] - 2021-05-04

### Added

* Added all public available classes to `__all__` variable.

### Fixed

* Added `boto3~=1.17` to `setup.py` as a dependency for addons.
* Added possibility to use self exported environment variables if not in pipeline environment.

## [0.9.3] - 2021-05-03

### Added

* Added possibility to exchange preconfigured gcip image in evaluate_git_tag_pep440_conformity function.
* Added contextmanager methods `__enter__`, `__exit__` to Pipeline class.

### Fixed

* Fixed `needs` bug with job name generation see #32811ff1

## [0.9.2] - 2021-04-30

### Information

* Skipped version 0.9.1 for testing purposes

### Added

* Added `core.job.PagesJob` class as special job for Gitlab Pages

## [0.9.0] - 2021-04-29

### Changed

* Compatibility to Python 3.7 is ensured. As Python 3.7 is the minimum default version in
  major distributions.

## [0.8.0] - 2021-04-29

### Added

* Added full API documentation of the `gcip.core.need` module.
* Added full API documentation of the `gcip.core.rule` module.
* Added API documentation to the `gcip.core.service` module.
* Added `job_name` and `job_stage` parameters to all job generating methods.
* **BREAKING** Added `gcip.core.Artifacts` and `gcip.core.ArtifactsReport` classes.
  Migration steps, search for `add_artifacts_paths` and replace with `artifacts.add_paths` in your project.
* Crane push addon build `latest` tag on branch `main` or `master`
* Updated PredefineImages.GCIP to use tag latest.

### Changed

* Internally use `copy.deepcopy()` to ensure uniqueness of job configurations.

## [0.7.0] - 2021-04-19

### Added

* Added `gcip.addon.container.job.trivy.scan_local_image` to scan local container images of vulnerabilities.
* Added set_config_file_path method to DockerClientConfig.
* Return own instance on each method call for DockerClientConfig instances.
* Added Registry class to `gcip.addons.container.registry` module. It contains constants of Container registries.
* Added `gcip.core.sequence.Sequence.initialize_artifacts_paths()` and `gcip.core.sequence.Sequence.override_artifacts_paths()`.
* Added defaulting to git tag or git branch for image_tag in `crane.push`.
* Added default `DockerClientConfig` in `crane.push`
* Added `|tee` to get `dive` output to stdout and to dive.txt. Updload dive.txt to GitLab artifacts store.
* Added `|tee` to get `trivy` output to stdout and to trivy.txt. Updload trivy.txt to GitLab artifacts store.
* Added new container sequence. Container sequence build, scans and pushes an container image.
* Added full API documentation of the `gcip.core.job` module.
* Added full API documentation of the `gcip.core.pipeline` module.
* Added full API documentation of the `gcip.core.image` module.
* Added full API documentation of the `gcip.core.include` module.
* Added documentation to the `gcip.core.variables` module.
* Added @properties to all public Job attributes.
* New addons: *aws* to allow receiving AWS account id and region.
* New `gcip.addons.container.registries.Registry.AWS()` allows getting an ECR URL to be used in pipeline.
* Added `crane.pull()` function to `gcip.addons.container.crane`.
* Added new gcip.addons.security.sops module and added sops_export_decrypted_values function to sops module.

### Changed

* Normalize config_file_path in `gcip.addons.container.config.DockerClientConfig`
* Line length check in flake8 linter to 160.
* **BREAKING** Removed arguments from DockerClientConfig constructor to set config file path, use `set_config_file_path` of DockerClientConfig instance.
* **BREAKING** Removed custom docker client config from kaniko job.
  Added DockerClientConfig as a optional client configuration to kaniko job.
  Simplyfied function and sorted initialisation tests an actuall composing of job.
* **BREAKING** Removed custom docker client config from kaniko job.
  Added DockerClientConfig as a optional client configuration to kaniko job.
  Simplyfied function and sorted initialisation tests an actuall composing of job.
* **BREAKING** Moved all function arguments of `crane.push` function to be keyword arguments.
* **BREAKING** Renamed `dst` to `dst_registry` in `crane.push` function.
* **BREAKING** Renamed `image_path` to `tar_path` keyword argument to aligne with kaniko module.
* **BREAKING** Renamed `dst` and `src` to `dst_registry` and `src_registry`.
* **BREAKING** `dive`: Replace "/" with "_" in image_name. Image names contains namespaces which are separated by "/" to ensure image name is a file instead of a directory structure.
* **BREAKING** `trivy`: Replace "/" with "_" in image_name. Image names contains namespaces which are separated by "/" to ensure image name is a file instead of a directory structure.
* **BREAKING** Renamed `gitlab_executor_image` to `kaniko_image` in `kaniko.execute()`. Moved argument to last argument in function signature.
* `kaniko`: Replaced "/" with "_" to convert image namspaces to filename instead of directory structure assigne it to image_path.
* `core.cache`: Changed PredefinedVariable from CI_PROJECT_PATH to CI_PROJECT_DIR to ensure its the directory instead of the "namespace" of the git repository.
* **BREAKING** Changed docker hub registry entry in `Registry` class.
* **BREAKING** Renamed all occurences of `namespace` to `stage`. Because 'stage' is what the current 'stage' really expresses. You could try following commands to align your
  gcip code with this breaking change:

  ```bash
  LC_ALL=C find . -type f ! -path './.git/*' ! -path '*/__pycache__/*' -exec sed -i '' s/Stage/Stage/g {} +
  LC_ALL=C find . -type f ! -path './.git/*' ! -path '*/__pycache__/*' -exec sed -i '' s/stage/stage/g {} +
  ```

### Removed

* Removed `date` call from dive job.
* Removed `gcip.core.pipeline.Pipeline.dump_yaml()` method. There is no need to print a pipeline to stdout. You should use `gcip.core.pipeline.Pipeline.write_yaml()` instead.
* Removed `-v` flag from from `gcp.addons.python.jobs.pip_install_requirements()`

### Fixed

* Several linter issues has been fixed
* Fixed kaniko build in gitlabci-local.sh
* Kankio job's tar_path behavior fixed. If you specified tar_path in `kaniko.execute()`, the `tar_path` was added to the same line as the executor. Now it gets added to a item bevor `execute`.
* Fixed PredefinedImages entrypoints for GitLab CI runner.
* Fixed crane image, `latest` image does not have `sh` available. Using `debug` tag.

## [0.6.1] - 2021-04-07

### Changed

* `PredefinedVariables` return in all cases a proxy object, which calls `os.environ` or `os.getenv` as late as possible.
  This helps when overriding (monkeypatching) variables in pytestes.

## [0.6.0] - 2021-04-06

### Added

* Added config.yml to .github dir to force using issue templates.
* Added gitlab_ci_environment_variables monkeypatch fixture. It allows patching environment variables.
* Added gitlab_ci_environmnet_variables fixture to tests.
* The gcip is now able to detect if two or more jobs would have the same name in the rendered pipeline
  and raises an ValueError to prevent undesirable pipeline behavior.
* Added new addon to check container optimization with `dive`
* Improved conftest.check() function. It tells the user how to create comparison files if the file not found exception.
* Improved `conftest.check()` function. Now AssertionError is handled, the user will get receive how to update comparison files.
* Added new class `PredefinedImages` in `gcip.addons.container`. Allows access to container images, that are widley used.
* Added new `class` which handels docker client config and renders it to a json string.
* Added `gcip.addon.container.job.crane` to allow copying container images between registries.
* Added `push` Job function to `gcip.addon.container.job.crane` to allow pushing local tarballs to remote registries.

### Changed

* **BREAKING** Renamed all occurences of 'job*sequence' to 'sequence'. Mainly this renames
  `gcip.core.job_sequence.JobSequence` to `gcip.core.sequence.Sequence`.
* Changed behavior how PredefinedVariables is handling environment variables.
  PredefinedVariables knows which environment variables are always present or under certain circumstances,
  like merge requests or if GitLab container registry is present.
  Variables marked with limited availabilty within official documentation returns String or None.
  All variables which are documented as always present return String.
* If the gcip code is executed outside a pipeline ($CI is empty) then for all expected `CI_*` variables
  a dummy string is returned instead of raising a KeyError.
* The `gcip.Pipeline` has now an `add_services()` method instead `add_service()` allowing to pass multiple
  services at once.
* Now jobs with hardcoded images, now using PredefinedImages images instead.

## [0.5.0] - 2021-03-16

### Added

* The CHANGELOG.md itself.
* Bug, Feature and Pull request templates (#9)

### Changed

* **BREAKING:** The signature of `gcip.addons.python.jobs.twine_upload()` has changed. The parameters `twine_repository_url`,
  `twine_username_env_var` and `twine_username_env_var` substitute the parameters `repository_url`, `user` and `varname_password`.
  Check the API documentation for the new parameters.
* **BREAKING:** Renamed all occurences of `pep404` to `pep440` as PEP 440 is the right specification number.

## [Template]

### Added

### Changed

### Deprecated

### Removed

### Fixed

### Security
