"""The core module contains all direct [Gitlab CI keyword](https://docs.gitlab.com/ee/ci/yaml/) representations."""

from typing import Dict

__author__ = "Thomas Steinbach"
__copyright__ = "Copyright 2020 DB Systel GmbH"
__credits__ = ["Thomas Steinbach", "Daniel von Eßen"]
# SPDX-License-Identifier: Apache-2.0
__license__ = "Apache-2.0"
__maintainer__ = "Thomas Steinbach"
__email__ = "thomas.t.steinbach@deutschebahn.com"

OrderedSetType = Dict[str, None]
