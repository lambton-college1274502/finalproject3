from gcip import Pipeline
from tests import conftest
from gcip.addons.container.jobs.docker import Push, Build


def test_default_docker_jobs():
    pipeline = Pipeline()

    pipeline.add_children(
        Build(repository="myspace/myimage"),
        Push(container_image="myspace/myimage"),
    )

    conftest.check(pipeline.render())
