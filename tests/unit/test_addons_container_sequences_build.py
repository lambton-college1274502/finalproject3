from tests import conftest
from gcip.core.pipeline import Pipeline
from gcip.addons.container.config import DockerClientConfig
from gcip.addons.container.registries import Registry
from gcip.addons.container.sequences.build import (
    FullContainerSequence,
)


def test_full_container_sequence():
    pipeline = Pipeline()
    dcc = DockerClientConfig()
    dcc.add_cred_helper(Registry.QUAY, "quay-login")
    s = FullContainerSequence(
        registry=Registry.QUAY,
    )
    s.kaniko_execute_job.docker_client_config = dcc
    s.crane_push_job.docker_client_config = dcc
    pipeline.add_children(s)
    conftest.check(pipeline.render())


def test_addons_container_sequences_full_container_sequence():
    pipeline = Pipeline()
    dcc = DockerClientConfig()
    dcc.add_auth(Registry.DOCKER)
    s = FullContainerSequence(registry=Registry.DOCKER)
    s.trivy_scan_job.set_image("custom/trivy:v1.2.3")
    s.kaniko_execute_job.build_args = {"first_arg": "foo", "second_arg": "bar"}
    pipeline.add_children(s)
    conftest.check(pipeline.render())
