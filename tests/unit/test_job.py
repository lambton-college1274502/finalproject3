import pytest

from gcip import (
    Job,
    Rule,
    Cache,
    PagesJob,
    WhenStatement,
    PredefinedVariables,
)
from tests import conftest
from gcip.core.artifacts import Artifacts


@pytest.fixture()
def rule():
    rule = Rule(
        if_statement=f"{PredefinedVariables.CI_COMMIT_REF_NAME} == main",
        when=WhenStatement.ALWAYS,
        allow_failure=True,
    )
    return rule


@pytest.fixture()
def job(rule):
    return Job(
        script=["date", f'echo "You are running on branch: ${PredefinedVariables.CI_COMMIT_REF_NAME}"'],
        stage="fixture_stage",
        name="job_name",
        image="busybox",
        allow_failure=True,
        cache=Cache(paths=["path/to/cache/"]),
        rules=[Rule(if_statement='echo "I am prepended" || true'), rule],
        artifacts=Artifacts("custom/path/to/artifact.txt"),
        tags=["custom", "docker"],
        variables={"ENV_VAR": "Hello", "CUSTOM": "World"},
        # This add_needs() call will result in an empty list,
        # this is because the Job() object is not added to a pipeline.
        needs=[Job(script="echo I am important", stage="needs", name="needs_job")],
    )


def test_job_render(job):
    conftest.check(job.render())


def test_job_properties(job):
    check_job_properties(job)


def test_job_modification(rule):
    job = Job(script="date", stage="fixture_stage", name="job_name")
    job.append_scripts(f'echo "You are running on branch: ${PredefinedVariables.CI_COMMIT_REF_NAME}"')
    job.set_image("busybox")
    job.set_allow_failure(True)
    job.set_cache(Cache(paths=["path/to/cache/"]))
    job.append_rules(rule)
    job.prepend_rules(Rule(if_statement='echo "I am prepended" || true'))
    job.artifacts.add_paths("custom/path/to/artifact.txt")
    job.add_tags("custom", "docker")
    job.add_variables(
        ENV_VAR="Hello",
        CUSTOM="World",
    )
    # This add_needs() call will result in an empty list,
    # this is because the Job() object is not added to a pipeline.
    job.add_needs(Job(script=f"echo I am needed by {job.name}", stage="needs", name="needs_job"))

    check_job_properties(job)


def check_job_properties(check_job: Job):
    assert check_job.name == "job-name-fixture-stage"
    assert check_job.stage == "fixture_stage"
    assert check_job.image.name == "busybox"
    assert check_job.variables == {"ENV_VAR": "Hello", "CUSTOM": "World"}
    assert check_job.tags == ["custom", "docker"]
    assert check_job.rules[0].render() == {"if": 'echo "I am prepended" || true'}
    assert check_job.rules[1].render() == {"allow_failure": True, "if": "my_awsome_feature_branch == main", "when": "always"}
    assert check_job.needs[0].name == "needs-job-needs"
    assert check_job.scripts[0] == "date"
    assert "custom/path/to/artifact.txt" in check_job.artifacts.paths
    assert check_job.cache.paths[0] == "./path/to/cache/"
    assert check_job.allow_failure is True


def test_set_tags(job):
    job.set_tags("new", "world")
    assert job.tags == ["new", "world"]


def test_job_exceptions():
    with pytest.raises(ValueError):
        Job(script="Neither name nor stage")
    with pytest.raises(AttributeError):
        Job(script={"Wrong": "Attribute, Type"}, name="test_script_attribute_exception")  # type: ignore


def test_pages_job(pipeline):
    pipeline.add_children(PagesJob())
    conftest.check(pipeline.render())
